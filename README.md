# Wetterhorn

Wetterhorn is a wlroots-based Wayland compositor inspired by XMonad. It has a
unique harness-plugin architecture to support hot-reloading of some code. The
goal is to have as much code as feasible hot-reloadable.

This is accomplished by Wetterhorn's architecture. It has 2 parts:

  - A harness, written in C, that provides the core of interacting with Wayland.
    It is based off tinywl, dwl, weston and others. The window management and
    event handling duties are stubbed out though, relying on a plugin to manage
    those.

  - A plugin, written in Haskell, which provides the "configuration" for
    handling events and window management. This plugin could theoretically be
    replaced by any other program implementing the stubs.

    The aim is for the plugin to contain as much logic as possible as it is
    likely to be written in a safe language.

This architecture provides some benefits.

  1. It provides speed where it counts by having the compositor bits being
     written in a language like C.

  2. It provides safety and expressiveness where it counts by having the logic
     and handlers written in a higher level language, like Haskell.

  3. Right now the harness and Haskell plugin are coupled together, but in the
     future the boundary between the plugin and harness can be generalized to
     allow different plugins to be used if one particularly does not like the
     haskell implementation.
  
  4. It enables hot-reloading. This is a huge benefit, and the main impetus for
     this architecture. With X11, the Window Manager was a separate binary that
     could be reloaded without killing the whole session. This is not the case
     for Wayland because the Window Manager and Compositor are the same binary.
     This is great for performance, but really bad for window managers like DWM
     and XMonad which rely on the hot-swappability as these WMs source code _is_
     their configuration.

     Now, when a changes is made to the plugin, it can be hot-reloaded by the
     harness, theoretically providing the best of both worlds in terms of
     performance and configuration expressiveness.
