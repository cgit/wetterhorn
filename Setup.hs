import Control.Monad (forM_, when)
import Distribution.Simple
import Distribution.Simple.Setup
import Distribution.Types.HookedBuildInfo
import System.Directory
import System.Environment (getArgs)
import System.FilePath
import System.Posix.Files (createSymbolicLink, removeLink)
import System.Process
import Text.Printf
import Data.Maybe (fromJust)

main = do
  putStrLn "Running Setup.hs"

  defaultMainWithHooks $
    simpleUserHooks
      { preConf = \_ conf -> do
          let buildPath = fromJust (flagToMaybe $ configDistPref conf)

          callCommand $
            printf "cd wlroots && meson setup %s -Dexamples=false --reconfigure"  (wlrootsDir buildPath)

          callCommand $
            printf "cmake -B %s -S harness"  (harnessDir buildPath)

          lnF ("../" ++ harnessDir buildPath) "harness/build"

          return emptyHookedBuildInfo,
        preBuild = \_ conf -> do
          let path = fromJust (flagToMaybe $ buildDistPref conf)

          callCommand $ printf "cd wlroots && ninja -C %s" (wlrootsDir path)

          callCommand $
            printf "make -C %s"  (harnessDir path)

          lnF (printf "%s/wtr_harness" (harnessDir path)) "wtr_harness"

          return emptyHookedBuildInfo,
        postCopy = \a f pd _ -> do
          forM_ (flagToMaybe (copyDistPref f)) $ \copyDest -> do

            let pluginFile = copyDest </> "build" </> "wtr.so" </> "wtr.so"
            lnF pluginFile "wtr.so"
      }
  where
    wlrootsDir :: String -> String
    wlrootsDir = printf "../%s/build/wlroots"

    harnessDir :: String -> String
    harnessDir = printf "%s/build/wtr_harness"

    lnF from to = do
      printf "%s -> %s\n" from to
      flip when (removeLink to) =<< doesExist to
      createSymbolicLink from to

    doesExist f = do
      b1 <- doesFileExist f
      b2 <- doesDirectoryExist f
      return (b1 || b2)
