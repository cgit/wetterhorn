/* Contains a structure, which contains functions to back-call into
 * the harness code. */

#ifndef __FOREIGN_INTERFACE
#define __FOREIGN_INTERFACE

#define EXPORT(a) a

typedef void *ctx_t;

typedef struct FOREIGN_INTERFACE {
  /* DO NOT ADD ANY UNEXPORTED VARIABLES HERE */

  /* The context, which needs to be passed to each function. This context is
   * opaque to the plugin and should not be changed. */
  EXPORT(ctx_t ctx);

  /* Requests the harness hot reload the current plugin. */
  EXPORT(void (*request_hot_reload)(ctx_t ctx));

  /* Requests the harness hot reload the current plugin. */
  EXPORT(void (*do_log)(ctx_t ctx, const char *str));

  /* Requestes that the whole system exit. Exits with the given return code. */
  EXPORT(void (*request_exit)(ctx_t ctx, int rc));

  /* Returns the seat associated with the server. */
  EXPORT(void *(*get_seat)(ctx_t ctx));
} foreign_interface_t;

#undef EXPORT

#endif /* __FOREIGN_INTERFACE */
