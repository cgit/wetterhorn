#pragma once

typedef void *opqst_t;

typedef enum {
  SURFACE_MAP = 0,
  SURFACE_UNMAP,
  SURFACE_DELETE,
} surface_event_t;
