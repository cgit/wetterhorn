#include "plugin.h"

#include <ctype.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

typedef void *dllib_t;

typedef void *opqst_t;

void shx(uint8_t *state, uint32_t sz) {
  uint32_t i = 0;
  while (i < sz) {
    for (int j = 0; j < 16; ++j) {
      if (i < sz) {
        printf("%02x ", (unsigned int)state[i]);
      } else {
        printf("   ");
      }
      ++i;
    }

    i -= 16;

    printf("   ");

    for (int j = 0; j < 16; ++j) {
      if (i < sz) {
        if (isprint(state[i]) && !isspace(state[i])) {
          printf("%c", state[i]);
        } else {
          printf(".");
        }
      } else {
        printf(" ");
      }
      ++i;
    }
    printf("\n");
  }
}

void run_plugin(int argc, char **argv, plugin_t *plugin) {
  fprintf(stderr, "Running plugin: %s\n", plugin->plugin_name);

  uint32_t sz;

  void* new_handle = dlopen(argv[2], RTLD_LAZY);
  printf("old plugin init: %p\n", plugin->plugin_name);
  if (new_handle) {
    plugin_hot_reload(argc, argv, new_handle, plugin);
    printf("new plugin init: %p\n", plugin->plugin_name);
  } else {
    fprintf(stderr, "Unable to open new library: %s\n", dlerror());
  }

  return;
}

int main_(int argc, char **argv) {
  if (!argv[0] && !argv[1]) {
    fprintf(stderr, "Missing argument.\n");
    return 1;
  }

  plugin_t plugin;
  if (load_plugin_from_file(argc, argv, argv[1], &plugin)) {
    fprintf(stderr, "Exiting due to other failures.\n");
    return 1;
  }
  plugin_cold_start(&plugin);

  run_plugin(argc, argv, &plugin);

  return 0;
}
