#!/usr/bin/env perl

$comment="";

print "#include <stdio.h>\n";
print "#include <dlfcn.h>\n";
print "#include <pthread.h>\n";
print "#include <string.h>\n";
print "#include \"plugin.h\"\n\n";

print "int load_plugin_from_dl_(dlhandle_t dl, plugin_t* plug)\n";
print "{\n";
print "  void* ptr;\n";
print "  int ret = 0;\n";
print "\n";
print "  const char** name = dlsym(dl, \"plugin_name\");\n";
print "  memset(plug, 0, sizeof(*plug));\n";
print "  if (name) {\n";
print "    plug->plugin_name = *name;\n";
print "  } else {\n";
print "    plug->plugin_name = NULL;\n";
print "  }\n";
print "  plug->state = NULL;\n";
print "  plug->library_handle = dl;\n";
print "\n";
while (<>) {
  if (/^\s*EXPORT/) {
    my $line = "$_";
    while (not ($line =~ /;$/)) {
      my $nextline = <STDIN>;
      last unless defined $nextline;

      $line="$line$nextline";
    }
    if ($line =~ /^\s*EXPORT\(\s*((?:\w|\s*\*\s*)+)\s*\(\*(\w+)\)\s*\((.*)\)\);/s) {
      print "\n";
      print "  ptr = dlsym(dl, \"$2\");\n";
      print "  if (!ptr) {\n";
      print "    fprintf(stderr, \"Plugin missing %s\\n\", \"$2\");\n";
      print "    ret |= 1;\n";
      print "  }\n";
      print "  plug->$2 = ptr;\n";
      $comment="";
    }
  }
}
print "\n  return ret;\n";
print "}\n";
