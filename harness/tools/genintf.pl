#!/usr/bin/env perl

$comment="";

print "#ifndef _PLUG_INTF\n";
print "#define _PLUG_INTF\n";
print "\n#include <stdint.h>\n";
print "\n#include <plugin_types.h>\n";

while (<STDIN>) {
  if (/^\s*\/\*/) {
    $_ =~ s/^\s*//;
    $comment="$_";
    next;
  }

  if (/^\s*\*/) {
    $_ =~ s/^\s*/ /;
    $comment="$comment$_";
    next;
  }

  if (/^\s*EXPORT_INCLUDE\((.*)\)/) {
    print "#include $1\n";
  } elsif (/^\s*EXPORT/) {
    my $line = "$_";
    while (not ($line =~ /;$/)) {
      my $nextline = <STDIN>;
      last unless defined $nextline;

      $line="$line$nextline";
    }
    if ($line =~ /^\s*EXPORT\(\s*((?:\w|\s*\*\s*)+)\s*\(\*(\w+)\)\s*\((.*)\)\);/s) {
      print "$comment";
      print "$1 $2($3);\n\n";
      $comment="";
    } elsif ($line =~ /^\s*EXPORT\((.*)\);/s) {
      print "$1\n";
    }
  }
}
print "#endif /* _PLUG_INTF */\n";
