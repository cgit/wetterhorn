module Config where

import Wetterhorn.Core.W
import Wetterhorn.Layout.Full
import Wetterhorn.Layout.Combine

config = defaultConfig {
  keyHook = wio . print,
  surfaceHook = wio . print,
  layout = WindowLayout Full
}

-- wetterhorn :: IO Wetterhorn
-- wetterhorn = initWetterhorn defaultConfig
