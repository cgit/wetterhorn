-- | Contains useful constraints and constraint combinators.
module Wetterhorn.Constraints where

-- | A null constraint. All types implement this.
class Unconstrained a

instance Unconstrained a

-- | Combines multiple constraints by 'And'ing them together.
class (c1 a, c2 a) => (&&&&) c1 c2 a

instance (c1 a, c2 a) => (&&&&) c1 c2 a
