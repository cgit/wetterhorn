module Wetterhorn.Core.KeyEvent
  ( KeyEvent (..),
    KeyState (..),
  )
where

import Data.Word (Word32)
import Foreign (Ptr)
import Wetterhorn.Foreign.WlRoots

data KeyState = KeyPressed | KeyReleased deriving (Show, Read, Eq, Enum, Ord)

data KeyEvent = KeyEvent
  { timeMs :: Word32,
    keycode :: Word32,
    state :: KeyState,
    modifiers :: Word32,
    keysym :: Word32,
    codepoint :: Char,
    device :: Ptr WlrInputDevice
  }
  deriving (Show, Ord, Eq)
