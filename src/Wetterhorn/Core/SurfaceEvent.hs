module Wetterhorn.Core.SurfaceEvent
  ( SurfaceEvent (..),
    SurfaceState (..),
  )
where

import Wetterhorn.Foreign.WlRoots

data SurfaceState = Map | Unmap | Destroy
  deriving (Eq, Ord, Show, Read, Enum)

data SurfaceEvent = SurfaceEvent
  { state :: SurfaceState,
    surface :: Surface
  }
  deriving (Eq, Ord, Show)
