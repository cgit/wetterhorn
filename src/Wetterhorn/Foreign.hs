module Wetterhorn.Foreign
  ( TypedIntPtr (..),
    toPtr,
    fromPtr,
  )
where

import Foreign (IntPtr, Ptr)
import qualified Foreign

toPtr :: TypedIntPtr a -> Ptr a
toPtr (TypedIntPtr ip) = Foreign.intPtrToPtr ip

fromPtr :: Ptr a -> TypedIntPtr a
fromPtr = TypedIntPtr . Foreign.ptrToIntPtr

newtype TypedIntPtr a = TypedIntPtr IntPtr
  deriving (Show, Read, Eq, Ord, Num)
