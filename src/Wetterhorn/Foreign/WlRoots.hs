module Wetterhorn.Foreign.WlRoots where

import Foreign (Ptr, Word32)

data WlrSeat

data WlrInputDevice

data WlrEventKeyboardKey

data WlrXdgSurface

data WlrXWaylandSurface

data Surface
  = XdgSurface (Ptr WlrXdgSurface)
  | XWaylandSurface (Ptr WlrXWaylandSurface)
  deriving (Show, Ord, Eq)

class ForeignSurface a where
  toSurface :: Ptr a -> Surface

instance ForeignSurface WlrXdgSurface where
  toSurface = XdgSurface

instance ForeignSurface WlrXWaylandSurface where
  toSurface = XWaylandSurface

foreign import ccall "wlr_seat_set_keyboard" wlrSeatSetKeyboard :: Ptr WlrSeat -> Ptr WlrInputDevice -> IO ()

foreign import ccall "wlr_seat_keyboard_notify_key"
  wlrSeatKeyboardNotifyKey ::
    Ptr WlrSeat -> Word32 -> Word32 -> Word32 -> IO ()
