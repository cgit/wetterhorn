module Wetterhorn.Layout.Full where

import Data.Data (Typeable)
import Data.Default
import Wetterhorn.Constraints
import Wetterhorn.Core.W

data Full = Full
  deriving (Read, Show, Typeable)

instance Default Full where
  def = Full

instance LayoutClass Full where
  type C Full = Unconstrained

  pureLayout (a : _) _ = [(a, RationalRect 1 1 1 1)]
  pureLayout _ _ = []
